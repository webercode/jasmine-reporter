This is just a simple wrapper around [jasmine][0] and [jasmine-console-reporter][1].

Since both dependencies have configuration, `jasmine-reporter.json` differs from the traditional `jasmine.json` and allows
you to configure both. Add `"test": "jasmine-reporter"` to `"scripts"` in `package.json` for easy access.

[0]: https://www.npmjs.com/package/jasmine
[1]: https://www.npmjs.com/package/jasmine-console-reporter
