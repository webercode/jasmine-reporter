const { ArgumentParser } = require('argparse');
const { join } = require('path');
const logger = require('./logger');

const parser = new ArgumentParser({
	version: require(join(__dirname, '..', 'package.json')).version,
	addHelp: true,
	description: ''
});
parser.addArgument(['-i', '--init'], {
	help: 'write out a default jasmine-reporter.json',
	action: 'storeTrue',
});
parser.addArgument(['-d', '--debug'], {
	help: 'print debug statements',
	action: 'storeTrue',
});
parser.addArgument(['file'], {
	help: 'specific file(s) to pass to jasmine',
	nargs: '*',
	defaultValue: [],
});

const args = parser.parseArgs();
if (args.debug) logger.levels.push('debug');

module.exports = { init: args.init, debug: args.debug, file: args.file };
logger.debug('cli args:', module.exports);
