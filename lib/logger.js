'use strict';

const chalk = require('chalk');
const util = require('util');

const ERROR_STR = chalk.red('ERROR');
const WARN_STR = chalk.yellow(' WARN');
const INFO_STR = chalk.green(' INFO');
const DEBUG_STR = chalk.blue('DEBUG');

function log(level, ...args)
{
	if (Logger.levels.indexOf(level) > -1)
	{
		args = args.map((v) => typeof v === 'object' ? util.inspect(v, true, null, true) : v);
		console.log(args.join(' '));
	}
}

class Logger
{
	static error(...args) { log('error', ERROR_STR, ...args); }

	static warn(...args) { log('warn', WARN_STR, ...args); }

	static info(...args) { log('info', INFO_STR, ...args); }

	static debug(...args) { log('debug', DEBUG_STR, ...args); }
}

Logger.levels = ['error', 'warn', 'info'];

module.exports = Logger;
