#!/usr/bin/env node

const args = require('./argparse');
const config = require('./config');
const logger = require('./logger');
const Jasmine = require('jasmine');
const JasmineConsoleReporter = require('jasmine-console-reporter');

let jrConfig;

try
{
	jrConfig = require(config.path);
}
catch (e)
{
	logger.error('use jasmine-reporter.js --init to initialize jasmine-reporter.json');
	logger.error(e.stack || e);
	process.exit(1);
}

let jasmine;

try
{
	// setup Jasmine
	jasmine = new Jasmine();
	jasmine.loadConfig(jrConfig.jasmine);
}
catch (e)
{
	logger.error('failed to load jasmine');
	logger.error(e.stack || e);
	process.exit(1);
}

try
{
	// setup console reporter
	const reporter = new JasmineConsoleReporter(jrConfig.reporter);

	// initialize and execute
	jasmine.env.clearReporters();
	jasmine.addReporter(reporter);
}
catch (e)
{
	logger.error('failed to load reporter');
	logger.error(e.stack || e);
	process.exit(1);
}

try
{
	jasmine.execute(args.file);
}
catch (e)
{
	logger.error('failed jasmine execution');
	logger.error(e.stack || e);
	process.exit(1);
}
