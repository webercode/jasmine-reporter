const logger = require('./logger');
const fs = require('fs');
const path = require('path');
const args = require('./argparse');

const appPath = path.resolve(__dirname).split(`${path.sep}node_modules`)[0];
const configPath = path.join(appPath, 'jasmine-reporter.json');

logger.debug('jasmine-reporter.json path:', configPath);

if (args.init)
{
	const defaultConfig = {
		jasmine: {
			spec_dir: 'spec',
			spec_files: ['**/*[sS]pec.js'],
			helpers: ['helpers/**/*.js'],
			random: false,
			seed: null,
			stopSpecOnExpectationFailure: false
		},
		reporter: {
			colors: 1,           // (0|false)|(1|true)|2
			cleanStack: 1,       // (0|false)|(1|true)|2|3
			verbosity: 4,        // (0|false)|1|2|(3|true)|4
			listStyle: 'indent', // "flat"|"indent"
			activity: false
		},
	};

	fs.writeFileSync(configPath, JSON.stringify(defaultConfig, null, 2));
	logger.info('config written to', configPath);
	process.exit();
}

module.exports = { path: configPath };
